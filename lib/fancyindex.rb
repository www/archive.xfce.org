require 'rubygems'
require 'sinatra'
require 'haml'

require File.join(File.dirname(__FILE__), 'models', 'archive')

module Fancyindex
  class Application < Sinatra::Base

    include Fancyindex::Models

    get '/howtomirror' do
      @title = "How to become a mirror for Xfce"
      @minute = rand(59)
      haml :howtomirror, :ugly => true
    end

    get '/*' do |requesturi|
      root_dir = '/var/www/archive.xfce.org/'
      path = File.join(root_dir, requesturi)

      if not File.directory?(path)
        @missingurl = File.join('/', requesturi)
        status 404
        haml :notfound
      else
        order = params[:O]
        order = 'A' unless ['A', 'D'].include?(order)

        col = params[:C]
        col = 'N' unless ['N', 'M', 'S'].include?(col)

        @dir = { 'N' => 'A', 'M' => 'A', 'S' => 'A' }
        @dir.each {|c, o| @dir[c] = 'D' if col == c and order == o}

        @archive = Archive.new(path, requesturi, col, order == 'D')
        @title = "Index of #{@archive.href if @archive}"
        @notice = 'Individual, and often more up-to-date, releases of the Xfce core packages can be found in <a href="/src/xfce">/src/xfce</a>!' if requesturi.match(/^xfce/)

        haml :index, :ugly => true
      end
    end
  end
end
