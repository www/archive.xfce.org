module Fancyindex
  module Models
    class Archive

      class Item
        attr_reader :archive
        attr_reader :name
        attr_reader :path
        attr_reader :is_dir

        def initialize(archive, name)
          @archive = archive
          @name = name
          @path = File.join(archive.path, name)
          @is_dir = File.directory?(@path)
        end

        def ==(other)
          other.is_a?(self.class) \
            and other.name == name \
            and other.archive == archive
        end

        def <=>(other)
          return 0 unless other.is_a?(self.class)

          # sort directories before files
          return (is_dir ? -1 : 1) if is_dir != other.is_dir

          if self.archive.sortcol == 'M'
            val = File.mtime(path) <=> File.mtime(other.path)
          elsif self.archive.sortcol == 'S'
            val = File.size(path) <=> File.size(other.path)
          else
            val = name <=> other.name
          end
          self.archive.sortdesc ? -val : val
        end

        def href
          File.join(self.archive.href, self.name)
        end

        def iconname
          if self.is_dir
            "folder"
          elsif self.name =~ /\.(bz2|gz|rpm|pkg|run)$/
            "archive"
          elsif self.name =~ /\.(md5|sha1)/
            "cert"
          else
            "default"
          end
        end

        def size
          size = File.size(self.path)
          count = 0
          while  size >= 1024 and count < 4
            size /= 1024.0
            count += 1
          end
          format("%.1f",size) + %w(B KB MB GB TB)[count]
        end

        def mtime
          File.mtime(self.path).strftime("%F %H:%M")
        end

        def mirrorlist
          if self.name =~ /\.(bz2|gz|rpm|pkg|run)$/
            self.href + '.mirrorlist'
          end
        end
      end

      attr_accessor :path
      attr_accessor :href
      attr_accessor :sortcol
      attr_accessor :sortdesc

      def initialize(path, href, column, desc)
        @path = path
        @href = File.join('/', href)
        @sortcol = column
        @sortdesc = desc
      end

      def list_items
        @items = []
        Dir.entries(self.path).each do |name|
          # skip default directory entries
          next if name == '.' or name == '..'

          # skip empty directories
          fullpath = File.join(self.path, name)
          next if File.directory?(fullpath) and Dir.entries(fullpath) == ['.', '..']

          @items << Archive::Item.new(self, name)
        end
        @items.sort!
      end

      def parent
        File.dirname(self.href) if not self.href.eql? "/"
      end
      
      def hreflink
        base = '/'
        string = "<a href=\"/\">archive.xfce.org</a>"
        self.href.split('/').each do |part|
          next if part.empty?
          base += "#{part}/"
          string += "/<a href=\"#{base}\">#{part}</a>"
        end
        string
      end
    end
  end
end
